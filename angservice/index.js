'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var chalk = require('chalk');

var AngCvcGenerator = yeoman.generators.Base.extend({



    promptUser: function() {
        var done = this.async();

        console.log('');
        console.log('');
        console.log('[ Generate Service / Factory ]');
        console.log('');

        try {
            this.appName = require(path.join(process.cwd(), 'bower.json')).name;
        } catch (e) {
            try {
                this.appName = require(path.join(process.cwd(), 'package.json')).name;
            } catch (e2) {
                this.appName = path.basename(process.cwd());
            }
        }

        var prompts = [{
              type: 'list',
              name: 'typeOfService',
              message: 'Serivice or factory?',
              choices: [{
                name: 'Service',
                value: 'service'
              },{
                name: 'Factory',
                value: 'factory'
              }]
              ,default: 1
        },{
            type: 'input',
            name: 'filesName',
            message: 'Name of the service?'
        },{
            type: 'confirm',
            name: 'appendService',
            message: 'Append \'Service\'/\'Factory\' to filename?',
            default: false
        },{
            type: 'input',
            name: 'detectedAppName',
            message: 'What is the Angular application name?',
            default: this.appName
        }];

        this.prompt(prompts, function(props) {
            this.filesName = props.filesName;
            this.appName = props.detectedAppName;
            this.appendService = props.appendService;
            this.typeOfService = props.typeOfService;

            if (this.typeOfService === 'service') {
            	this.appendText = 'Service';
            } else if (this.typeOfService === 'factory') {
            	this.appendText = 'Factory';
            }

            done();
        }.bind(this));
    },
    scaffoldFolders: function() {
        console.log('');
        this.mkdir("app");
        this.mkdir("app/scripts");
        this.mkdir("app/scripts/services");

    },
    copyMainFiles: function() {

        var context = {
            appName: this.appName,
            filesName: this.filesName,
            serviceName: (this.appendService ? this.filesName+this.appendText : this.filesName),
            typeOfService: this.typeOfService
        };

        this.template("service.js", "app/scripts/services/" + this.filesName + (this.appendService ? this.appendText : '') + ".js", context);


    },
    addToIndex: function(){
        /*var path = "app/index.html",
        indexFile = this.readFileAsString(path);
        indexFile = indexFile.replace(/(\r\n|\n|\r)/gm,"");
        //var re = new RegExp('<!-- build:js\(\{\.tmp,app\}\) scripts\/scripts\.js([\s\S]*?)<!-- endbuild -->', 'gm');
        var re = new RegExp('<\!-- build:js\(\{\.tmp,app\}\) scripts\/scripts\.js([\s\S]*?)<\!-- endbuild -->', 'gm');
        var myArray = re.exec(indexFile);

        //var pattern = '/<!-- build:js\({.tmp,app}\) scripts/scripts.js([\s\S]*?)<!-- endbuild -->/gm';
        //var nameList = indexFile.match(pattern);

        var t = '<script src="app/scripts/controllers/' + this.filesName + (this.prependController ? 'Controller' : '') + '.js"></script>';

        console.log(indexFile);
        console.log(myArray);

        //this.write("app/menu.html", menu);*/
    },
    addEmptyLine: function() {
        console.log('');
		console.log('Copy and paste this into your index.html: <script src="scripts/services/' + this.filesName + (this.appendService ? this.appendText : '') + '.js"></script>');

        console.log('');
    }
});

module.exports = AngCvcGenerator;


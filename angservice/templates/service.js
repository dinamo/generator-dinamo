'use strict';

angular.module('<%= appName %>')
    .<%= typeOfService %>('<%= serviceName %>', ['$q', function ($q) {

        var myVars = {
            var1: true,
            var2: false
        };

        var myFunction = function() {
        	return true;
        }

        return {
        	myVars: myVars,
        	myFunction: myFunction
        };

    }]);

'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var chalk = require('chalk');

var AngCvcGenerator = yeoman.generators.Base.extend({



    promptUser: function() {
        var done = this.async();

        console.log('');
        console.log('');
        console.log('[ Generate Directive ]');
        console.log('');

        try {
            this.appName = require(path.join(process.cwd(), 'bower.json')).name;
        } catch (e) {
            try {
                this.appName = require(path.join(process.cwd(), 'package.json')).name;
            } catch (e2) {
                this.appName = path.basename(process.cwd());
            }
        }


        var prompts = [{
            type: 'input',
            name: 'filesName',
            message: 'Name of the directive?'
        },{
            type: 'confirm',
            name: 'appendDirective',
            message: 'Append \'Directive\' to filename?',
            default: false
        },{
            type: 'input',
            name: 'detectedAppName',
            message: 'What is the Angular application name?',
            default: this.appName
        }];

        this.prompt(prompts, function(props) {
            this.filesName = props.filesName;
            this.appName = props.detectedAppName;
            this.appendDirective = props.appendDirective;

            done();
        }.bind(this));
    },
    scaffoldFolders: function() {
        console.log('');
        this.mkdir("app");
        this.mkdir("app/scripts");
        this.mkdir("app/scripts/directives");

    },
    copyMainFiles: function() {

        var context = {
            appName: this.appName,
            filesName: this.filesName,
            directiveName: (this.appendDirective ? this.filesName+'Directive' : this.filesName)
        };

        this.template("directive.js", "app/scripts/directives/" + this.filesName + (this.appendDirective ? 'Directive' : '') + ".js", context);


    },
    addToIndex: function(){
        /*var path = "app/index.html",
        indexFile = this.readFileAsString(path);
        indexFile = indexFile.replace(/(\r\n|\n|\r)/gm,"");
        //var re = new RegExp('<!-- build:js\(\{\.tmp,app\}\) scripts\/scripts\.js([\s\S]*?)<!-- endbuild -->', 'gm');
        var re = new RegExp('<\!-- build:js\(\{\.tmp,app\}\) scripts\/scripts\.js([\s\S]*?)<\!-- endbuild -->', 'gm');
        var myArray = re.exec(indexFile);

        //var pattern = '/<!-- build:js\({.tmp,app}\) scripts/scripts.js([\s\S]*?)<!-- endbuild -->/gm';
        //var nameList = indexFile.match(pattern);

        var t = '<script src="app/scripts/controllers/' + this.filesName + (this.prependController ? 'Controller' : '') + '.js"></script>';

        console.log(indexFile);
        console.log(myArray);

        //this.write("app/menu.html", menu);*/
    },
    addEmptyLine: function() {
        console.log('');
		console.log('Copy and paste this into your index.html: <script src="scripts/directives/' + this.filesName + (this.appendDirective ? 'Directive' : '') + '.js"></script>');

        console.log('');
    }
});

module.exports = AngCvcGenerator;


'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var chalk = require('chalk');

var AngCvcGenerator = yeoman.generators.Base.extend({



    promptUser: function() {
        var done = this.async();

        console.log('');
        console.log('');
        console.log('[ Generate Partial with optional Controller and Sass file ]');
        console.log('');

        try {
            this.appName = require(path.join(process.cwd(), 'bower.json')).name;
        } catch (e) {
            try {
                this.appName = require(path.join(process.cwd(), 'package.json')).name;
            } catch (e2) {
                this.appName = path.basename(process.cwd());
            }
        }

        var prompts = [{
            type: 'input',
            name: 'filesName',
            message: 'Name of the partial?'
        },{
            type: 'confirm',
            name: 'appendPartial',
            message: 'Append "Partial" to partial filename?',
            default: false
        },{
              type: 'list',
              name: 'createController',
              message: 'Should I create a controller for this partial?',
              choices: [{
                name: 'Yes',
                value: 'createController'
              },{
                name: 'Yes, and append \'Controller\'',
                value: 'createControllerAppend'
              },{
                name: 'No thanks',
                value: 'noController'
              }]
              ,default: 2
        },{
              type: 'list',
              name: 'createStyle',
              message: 'Should I create a style for this partial?',
              choices: [{
                name: 'Yes',
                value: 'createStyle'
              },{
                name: 'Yes, and append \'Partial\'',
                value: 'createStyleAppend'
              },{
                name: 'No thanks',
                value: 'noStyle'
              }]
              ,default: 2
        },{
            type: 'input',
            name: 'detectedAppName',
            message: 'What is the Angular application name?',
            default: this.appName
        }];

        this.prompt(prompts, function(props) {
            this.filesName = props.filesName;
            this.appName = props.detectedAppName;
            this.appendPartial = props.appendPartial;
            this.createController = props.createController;
            this.createStyle = props.createStyle;
            done();
        }.bind(this));
    },
    scaffoldFolders: function() {
        console.log('');
        this.mkdir("app");

        if (this.createController !== 'noController') {
            this.mkdir("app/scripts");
            this.mkdir("app/scripts/controllers/partials");
        }

        this.mkdir("app/partials");

        if (this.createStyle !== 'noStyle') {
            this.mkdir("app/styles");
            this.mkdir("app/styles/partials");
        }
    },
    copyMainFiles: function() {

        var context = {
            appName: this.appName,
            filesName: this.filesName,
            controllerName: (this.createController === 'createControllerAppend' ? this.filesName+'Controller' : this.filesName)
        };

        if (this.createController === 'createController' || this.createController === 'createControllerAppend') {
            this.template("controller.js", "app/scripts/controllers/partials/" + this.filesName + (this.createController === 'createControllerAppend' ? 'Controller' : '') + ".js", context);
        }

        if (this.createStyle === 'createStyle' || this.createStyle === 'createStyleAppend') {
            this.template("style.scss", "app/styles/partials/" + this.filesName + (this.createStyle === 'createStyleAppend' ? 'Partial' : '') + ".scss", context);
        }

        if (this.createController === 'noController') {
            this.template("partial_nocontroller.html", "app/partials/" + this.filesName + (this.appendPartial ? 'Partial' : '') + ".html", context);
        } else {
            this.template("partial.html", "app/partials/" + this.filesName + (this.appendPartial ? 'Partial' : '') + ".html", context);
        }

    },
    addToIndex: function(){
        /*var path = "app/index.html",
        indexFile = this.readFileAsString(path);
        indexFile = indexFile.replace(/(\r\n|\n|\r)/gm,"");
        //var re = new RegExp('<!-- build:js\(\{\.tmp,app\}\) scripts\/scripts\.js([\s\S]*?)<!-- endbuild -->', 'gm');
        var re = new RegExp('<\!-- build:js\(\{\.tmp,app\}\) scripts\/scripts\.js([\s\S]*?)<\!-- endbuild -->', 'gm');
        var myArray = re.exec(indexFile);

        //var pattern = '/<!-- build:js\({.tmp,app}\) scripts/scripts.js([\s\S]*?)<!-- endbuild -->/gm';
        //var nameList = indexFile.match(pattern);

        var t = '<script src="app/scripts/controllers/' + this.filesName + (this.prependController ? 'Controller' : '') + '.js"></script>';

        console.log(indexFile);
        console.log(myArray);

        //this.write("app/menu.html", menu);*/
    },
    addEmptyLine: function() {
        console.log('');
        if (this.createController !== 'noController') {
            console.log('Copy and paste this into your index.html: <script src="scripts/controllers/partials/' + this.filesName + (this.createController === 'createControllerAppend' ? 'Controller' : '') + '.js"></script>');
        }
        if (this.createStyle !== 'noStyle') {
            console.log('Copy and paste this into main.scss: @import \'partials/' + this.filesName + (this.createStyle === 'createStyleAppend' ? 'Partial' : '') + '\';');
        }
        console.log('');
    }
});

module.exports = AngCvcGenerator;


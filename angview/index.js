'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var chalk = require('chalk');

var AngCvcGenerator = yeoman.generators.Base.extend({



    promptUser: function() {
        var done = this.async();

        console.log('');
        console.log('');
        console.log('[ Generate View with Controller and Sass file ]');
        console.log('');

        try {
            this.appName = require(path.join(process.cwd(), 'bower.json')).name;
        } catch (e) {
            try {
                this.appName = require(path.join(process.cwd(), 'package.json')).name;
            } catch (e2) {
                this.appName = path.basename(process.cwd());
            }
        }

        var prompts = [{
            type: 'input',
            name: 'filesName',
            message: 'Name of the view?'
        },{
            type: 'confirm',
            name: 'prependController',
            message: 'Append \'Controller\' to controller name?',
            default: false
        },{
            type: 'confirm',
            name: 'prependView',
            message: 'Append \'View\' to view and style filename?',
            default: false
        },{
            type: 'input',
            name: 'detectedAppName',
            message: 'What is the Angular application name?',
            default: this.appName
        }];

        this.prompt(prompts, function(props) {
            this.filesName = props.filesName;
            this.appName = props.detectedAppName;
            this.prependController = props.prependController;
            this.prependView = props.prependView;
            done();
        }.bind(this));
    },
    scaffoldFolders: function() {
        console.log('');
        this.mkdir("app");
        this.mkdir("app/scripts");
        this.mkdir("app/scripts/controllers");
        this.mkdir("app/scripts/controllers/views");
        this.mkdir("app/views");
        this.mkdir("app/styles");
        this.mkdir("app/styles/views");
    },
    copyMainFiles: function() {

        var context = {
            appName: this.appName,
            filesName: this.filesName,
            controllerName: (this.prependController ? this.filesName+'Controller' : this.filesName)
        };

        this.template("controller.js", "app/scripts/controllers/views/" + this.filesName + (this.prependController ? 'Controller' : '') + ".js", context);
        this.template("view.html", "app/views/" + this.filesName + (this.prependView ? 'View' : '') + ".html", context);
        this.template("style.scss", "app/styles/views/" + this.filesName + (this.prependView ? 'View' : '') + ".scss", context);
    },
    addToIndex: function(){
        /*var path = "app/index.html",
        indexFile = this.readFileAsString(path);
        indexFile = indexFile.replace(/(\r\n|\n|\r)/gm,"");
        //var re = new RegExp('<!-- build:js\(\{\.tmp,app\}\) scripts\/scripts\.js([\s\S]*?)<!-- endbuild -->', 'gm');
        var re = new RegExp('<\!-- build:js\(\{\.tmp,app\}\) scripts\/scripts\.js([\s\S]*?)<\!-- endbuild -->', 'gm');
        var myArray = re.exec(indexFile);

        //var pattern = '/<!-- build:js\({.tmp,app}\) scripts/scripts.js([\s\S]*?)<!-- endbuild -->/gm';
        //var nameList = indexFile.match(pattern);

        var t = '<script src="app/scripts/controllers/' + this.filesName + (this.prependController ? 'Controller' : '') + '.js"></script>';

        console.log(indexFile);
        console.log(myArray);

        //this.write("app/menu.html", menu);*/
    },
    addEmptyLine: function() {
        console.log('');
        console.log('Copy and paste this into your index.html: <script src="scripts/controllers/views/' + this.filesName + (this.prependController ? 'Controller' : '') + '.js"></script>');
        console.log('Copy and paste this into main.scss: @import \'views/' + this.filesName + (this.prependView ? 'View' : '') + '\';');
        console.log('');
    }
});

module.exports = AngCvcGenerator;


'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var yosay = require('yosay');
var chalk = require('chalk');

var DinamoGenerator = yeoman.generators.Base.extend({
  init: function () {
    this.pkg = require('../package.json');

    this.on('end', function () {
      if (!this.options['skip-install']) {
        this.installDependencies();
      }
    });
  },

  askFor: function () {

    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the Dinamo Workflow generator'
    ));

    this.log(':angview    - Generate View with Controller and Sass file. [Angular]');
    this.log(':angpartial - Generate Partial with optional Controller and Sass file. [Angular]');
  }
});

module.exports = DinamoGenerator;

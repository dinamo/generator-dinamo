# generator-dinamo

> [Yeoman](http://yeoman.io) generator


## Getting Started

### What is Dinamo Generator?

It is a generator for Yeoman adapted to the workflow of the developers at Dinamo. In essence this means that it´s not really useful for anybody else.

```bash
npm install generator-dinamo
```


### dinamo:angview

Generates a view with controller and SASS file for Angular projects.

```bash
yo dinamo:angview
```


### dinamo:angpartial

Generates a partial with controller and sass file for Angular projects.

```bash
yo dinamo:angpartial
```


### dinamo:angservice

Generates a service or factory for Angular projects.

```bash
yo dinamo:angservice
```


### dinamo:angdirective

Generates a directive for Angular projects.

```bash
yo dinamo:angdirective
```


### dinamo:angevergreen

Adds the evergreen service to Angular projects.

```bash
yo dinamo:angevergreen
```



## License

MIT

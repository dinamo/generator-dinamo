'use strict';

/*

	isEvergreen.check({
      "custom": {
          "styles": ["animation"],
          "html5": ["canvas"]
      },
    })


	returns:
		isEvergreen: true | false
		customRequirements: true | false

		Modernizr: The Modernizr object
		results: Detailed results
 */

angular.module('<%= appName %>')
    .factory('isEvergreen', ['$q', '$log', function ($q, $log) {

        var testObj = {
            'custom': {
                'styles': [],
                'html5': []
            },
            'styles': [
                    'animation',
                    'transition',
                    'textShadow'
                ],
            'html5': [
                    'canvas',
                    'history',
                    'video',
                    'svg'
                ]
        };

        var check = function(customData) {

            var returnObj = {
                'isEvergreen': true,
                'customRequirements': true,
                'Modernizr': null,
                'results': {
                    'custom': {
                        'success': [],
                        'failed': []
                    },
                    'predefined': {
                        'success': [],
                        'failed': []
                    }
                }

            };

            returnObj.Modernizr = Modernizr;

            /* Prepare custom elements */
            if (isArray(customData.custom.styles)) {
                for (var i = 0; i < customData.custom.styles.length; i++) {
                    testObj.custom.styles.push(customData.custom.styles[i]);
                }
            }

            if (isArray(customData.custom.html5)) {
                for (var i = 0; i < customData.custom.html5.length; i++) {
                    testObj.custom.html5.push(customData.custom.html5[i]);
                }
            }


            /**
            *
            * Custom checks
            *
            **/

            /* Styles */
            for (var i = 0; i < testObj.custom.styles.length; i++) {
                var test = Modernizr.testAllProps(testObj.custom.styles[i]);
                if (test) {
                    returnObj.results.custom.success.push(testObj.custom.styles[i]);
                } else {
                    returnObj.results.custom.failed.push(testObj.custom.styles[i]);
                    returnObj.customRequirements = false;
                }
            }

            /* HTML5 */
            for (var i = 0; i < testObj.custom.html5.length; i++) {
                var test = Modernizr[testObj.custom.html5[i]];
                if (test) {
                    returnObj.results.custom.success.push(testObj.custom.html5[i]);
                } else {
                    returnObj.results.custom.failed.push(testObj.custom.html5[i]);
                    returnObj.customRequirements = false;
                }
            }


            /**
            *
            * Predefined checks
            *
            **/

            /* Styles */
            for (var i = 0; i < testObj.styles.length; i++) {
                var test = Modernizr.testAllProps(testObj.styles[i]);
                if (test) {
                    returnObj.results.predefined.success.push(testObj.styles[i]);
                } else {
                    returnObj.results.predefined.failed.push(testObj.styles[i]);
                    returnObj.isEvergreen = false;
                }
            }

            /* HTML5 */
            for (var i = 0; i < testObj.html5.length; i++) {
                var test = Modernizr[testObj.html5[i]];
                if (test) {
                    returnObj.results.predefined.success.push(testObj.html5[i]);
                } else {
                    returnObj.results.predefined.failed.push(testObj.html5[i]);
                    returnObj.isEvergreen = false;
                }
            }



            return returnObj;
        };

        function isArray(o) {
          return Object.prototype.toString.call(o) === '[object Array]';
        }

        return {
        	check: check,
            testObj: testObj
        };

    }]);

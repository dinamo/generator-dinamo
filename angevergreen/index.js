'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var chalk = require('chalk');

var AngCvcGenerator = yeoman.generators.Base.extend({



    promptUser: function() {
        var done = this.async();

        console.log('');
        console.log('');
        console.log('[ Add isEvergreen to project ]');
        console.log('');

        try {
            this.appName = require(path.join(process.cwd(), 'bower.json')).name;
        } catch (e) {
            try {
                this.appName = require(path.join(process.cwd(), 'package.json')).name;
            } catch (e2) {
                this.appName = path.basename(process.cwd());
            }
        }


        var prompts = [{
            type: 'input',
            name: 'detectedAppName',
            message: 'What is the Angular application name?',
            default: this.appName
        }];

        this.prompt(prompts, function(props) {
            this.appName = props.detectedAppName;

            done();
        }.bind(this));
    },
    scaffoldFolders: function() {
        console.log('');
        this.mkdir("app");
        this.mkdir("app/scripts");
        this.mkdir("app/scripts/services");

    },
    copyMainFiles: function() {

        var context = {
            appName: this.appName,
        };

        this.template("isevergreen.js", "app/scripts/services/isevergreen.js", context);


    },
    addEmptyLine: function() {
        console.log('');
		console.log('Copy and paste this into your index.html: <script src="scripts/services/isevergreen.js"></script>');
        console.warn('If you haven´t already, you must do: bower install Modernizr --save-dev');
        console.log('');
    }
});

module.exports = AngCvcGenerator;

